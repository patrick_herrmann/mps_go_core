package utils

import (
	"testing"
)

// TestUnmarshal simply tests if the a well formatted yaml content is
// properly unmarshalled.
func TestUnmarshal(t *testing.T) {
	t.Log("Testing unmarshalling a well formatted yaml content")
	cf := GetConfig()
	err := cf.Unmarshal([]byte(
		`a: Easy!
b:
  c: 2
  d: [3, 4]`))
	if err != nil {
		t.Fatalf("Could not unmarshal the configuration set: %v", err)
	}
	if value, err := cf.Get("a"); err != nil {
		t.Fatalf("Error unmarshalling the configuration set: key 'a' has no value - %v'", err)
	} else if value != "Easy!" {
		t.Fatalf("Error unmarshalling the configuration set: key 'a' value is wrong - %s'", value)
	}
}

// TestLoadingAndGettingDefaultConfiguration checks if the properties set in
// the default app configuration file:
//	./app_config.yaml
// are properly unmarshalled and stored into a Config instance and if they
// can be properly retrieved.
func TestLoadingAndGettingDefaultConfiguration(t *testing.T) {
	t.Log("Testing unmarshalling and storing the properties in the default configuration file")
	cf := GetConfig()
	if pval, err := cf.Get("prop1"); err != nil {
		t.Fatalf("Cannot get value for property prop1: %v.", err)
	} else if pval != "value1" {
		t.Fatalf("Value for property prop1 is not the expected one (value1): %s.", pval)
	}

	if pval, _ := cf.Get("prop3", "prop3.1", "prop3.1.1"); pval != "value3.1.1" {
		t.Fatalf("Value for property prop3/prop3.1/prop3.1.1 is not the expected one (value3.1.1): %v.", pval)
	}
	if pval, _ := cf.Get("prop3", "prop3.1", "prop3.1.2"); pval != 312 {
		t.Fatalf("Value for property prop3/prop3.1/prop3.1.2 is not the expected one (123): %v.", pval)
	}
	if pval, _ := cf.Get("prop3", "prop3.1", "prop3.1.3"); pval != false {
		t.Fatalf("Value for property prop3/prop3.1/prop3.1.2 is not the expected one (false): %v.", pval)
	}
	if _, err := cf.Get("prop3", "prop3.1", "prop3.1.4"); err == nil {
		t.Fatalf("Value for property prop3/prop3.1/prop3.1.4 shouldn't exist.")
	}
}

func TestGettingPropertyWithFallback(t *testing.T) {
	t.Log("Testing fallbacking to a default value if a property does not exist in a configuration")
	cf := GetConfig()
	if pval := cf.GetWithFallback("OK", "prop_1"); pval != "OK" {
		t.Fatalf("Fallback value for a property that does not exist is not the expected one: %v.", pval)
	}
}

func TestGettingAConfigurationSubsetAsAConfig(t *testing.T) {
	t.Log("Testing wrapping a subset of a configuration into a new Config instance.")
	cf := GetConfig()
	// Trying w/ one level down.
	scf := cf.GetConfigForTags("prop3")
	if _, err := scf.Get("prop3.1"); err != nil {
		t.Fatalf("Subset of the main configuration set does not contain the expected property: %v.", err)
	}
	// Trying w/ two levels down.
	scf = cf.GetConfigForTags("prop3", "prop3.1")
	if _, err := scf.Get("prop3.1.1"); err != nil {
		t.Fatalf("Subset of the main configuration set does not contain the expected property: %v.", err)
	}
}
