/*
utils package provides a standardized logger that self-configures and makes
it simplier to be used across the application.
*/
package utils

import (
	"fmt"
	"os"
	"sync"

	"github.com/sirupsen/logrus"
)

const (
	FATAL             int = 0
	ERROR             int = 1
	WARN              int = 2
	INFO              int = 3
	DEBUG             int = 4
	TRACE             int = 10
	UNSET_LOGGER_NAME     = "No name"
)

// Logger is the interface to actually log some information to a defined
// I/O writer.
type Logger struct {
	name        string
	level       int
	initialized bool
	config      *Config
	ulogger     *logrus.Logger
}

var loggers = make(map[string](*Logger))
var syncLoggers sync.Mutex

/*
GetLogger returns a logger for the given name

If there is no logger for the given name yet created, this function creates
one and assigns it to this name. Next calls for the same name will return
that instantiated logger, for improved performances.

The log configuration (level, etc.) is read from the Config.GetConfig
function; it should be set as follow in the app_config.yaml file:
	logger:
		level: [TRACE, DEBUG, INFO, WARN, ERROR, FATAL]

This functions is thread-safe.
*/
func GetLogger(name string) *Logger {
	syncLoggers.Lock()
	if _, ok := loggers[name]; !ok {
		// No logger matching the given name yet
		l := &Logger{
			config: GetConfig(),
		}
		l.initializeLogger(name)
		loggers[name] = l
	}
	syncLoggers.Unlock()
	return loggers[name]
}

/*
GetLoggerWithConfig gets a logger from the given configuration.

It has to be organised as follow:
	logger:
		name: "name of the logger"
		level: "TRACE"
*/
func GetLoggerWithConfig(cfg Config) *Logger {
	name := cfg.GetWithFallback(UNSET_LOGGER_NAME, "logger", "name")
	logger := GetLogger(name.(string))
	logger.config = &cfg
	return logger
}

// Name gets or sets the logger's name. This name is the one attached to
// every message logged with the logger.
//
// If the given name is nil, then this function returns the name previously
// set for the logger. Otherwise, it sets the new name for the logger and
// returns it.
func (l *Logger) Name(name ...string) string {
	if len(name) > 0 {
		l.name = name[0]
	}
	return l.name
}

func (l *Logger) Trace(function string, format string, args ...interface{}) {
	l.doLog(TRACE, function, format, args...)
}

func (l *Logger) Debug(function string, format string, args ...interface{}) {
	l.doLog(DEBUG, function, format, args...)
}

// Info logs the message consisting of the given format and the extra
// arguments args with the information level.
func (l *Logger) Info(function string, format string, args ...interface{}) {
	l.doLog(INFO, function, format, args...)
}

func (l *Logger) Warn(function string, format string, args ...interface{}) {
	l.doLog(WARN, function, format, args...)
}

func (l *Logger) Error(function string, format string, args ...interface{}) {
	l.doLog(ERROR, function, format, args...)
}

func (l *Logger) Fatal(function string, format string, args ...interface{}) {
	l.doLog(FATAL, function, format, args...)
}

// init initializes the logger from configurations, if not yet initialized
func (l *Logger) initializeLogger(name string) {
	if !l.initialized {
		switch level := l.config.GetWithFallback("DEBUG", "logger", "level"); level {
		case "TRACE":
			l.level = TRACE
		case "DEBUG":
			l.level = DEBUG
		case "INFO":
			l.level = INFO
		case "WARN":
			l.level = WARN
		case "ERROR":
			l.level = ERROR
		case "FATAL":
			l.level = FATAL
		default:
			l.level = DEBUG
		}
		l.name = name
		l.ulogger = logrus.New()
		// Log as JSON instead of the default ASCII formatter.
		l.ulogger.SetFormatter(&logrus.JSONFormatter{})

		// Output to stdout instead of the default stderr
		l.ulogger.SetOutput(os.Stdout)

		// Only log the warning severity or above.
		l.ulogger.SetLevel(logrus.TraceLevel)
		l.initialized = true
	}
}

func (l Logger) doLog(level int, function string, format string, args ...interface{}) {
	// If the given level is any level below the level defined for this
	// logger, the message is logged; otherwise, no resource is spent
	// logging the message.
	if level <= l.level {
		str := fmt.Sprintf(format, args...)
		fields := logrus.Fields{"name": l.name, "function": function}
		switch level {
		case FATAL:
			l.ulogger.WithFields(fields).Fatal(str)
		case ERROR:
			l.ulogger.WithFields(fields).Error(str)
		case WARN:
			l.ulogger.WithFields(fields).Warn(str)
		case INFO:
			l.ulogger.WithFields(fields).Info(str)
		case DEBUG:
			l.ulogger.WithFields(fields).Debug(str)
		default:
			l.ulogger.WithFields(fields).Trace(str)
		}
	}
}
