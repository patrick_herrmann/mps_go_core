package utils

import (
	"math"
	"math/rand"
	"time"
)

/*
Any struct is an underlying type for any kind of type that needs both, by
default, a Logger and a Config.

When Any should be the underlying implementation of another struct type, it
should be composed into that other struct:
	type Obj struct {
		... // Specific member attributes
		Any
	}
This is called struct composition: it simulates inheritance.

If obj is an instance of Obj, as follow:
	obj := &Obj{...}
It's possible to call the member attributes of Any:
	obj.Logger.Info("Information message")
*/
type Any struct {
	Logger *Logger
	Config *Config
}

/*
Init instantiates the Any type.

The given tag is used to initialise the logger attached to the type. This
means that using the current Any instance logger will cause the logs to be
attached to that tag. If there are subtags, the last one of those will be
used instead of the given tag.

The given tag and subtags are also used to initialise the configuration
attached to the Any instance. The Any instance is initialised with the set
of properties matching the given tag / subtags combination.
*/
func (any *Any) Init(tag string, subtags ...string) {
	if len(subtags) > 0 {
		any.Logger = GetLogger(subtags[len(subtags)-1])
	} else {
		any.Logger = GetLogger(tag)
	}
	any.Config = GetConfigForTag(tag, subtags...)
}

/*
InitWithConfig instantiates the Any type with the given configuration set.

From a Logger perspective, the configuration is then taken from the set of
sub-properties attached to the tag "logger".
*/
func (any *Any) InitWithConfig(cfg Config) {
	any.Config = &cfg
	any.Logger = GetLoggerWithConfig(cfg)
}

/*
Same as Any, but this one comes additionally with an unique identifier, that
is the result of picking a pseudo-random number between 0 and math.MaxInt32
and multiplying it by the current time in milliseconds:
	Id = int64(rand.Int31n(math.MaxInt32)) * time.Now().UnixMilli()
*/
type AnyWithId struct {
	Id int64
	Any
}

/*
Init initialises the identifier for the object and then calls the same
function on Any:
	Any.Init(tag, subtags)
*/
func (any *AnyWithId) Init(tag string, subtags ...string) {
	any.Any.Init(tag, subtags...)
	any.Id = int64(rand.Int31n(math.MaxInt32)) * time.Now().UnixMilli()
}

/*
Init initialises the identifier for the object and then calls the same
function on Any:
	Any.InitWithConfig(cfg)
*/
func (any *AnyWithId) InitWithConfig(cfg Config) {
	any.Any.InitWithConfig(cfg)
	any.Id = int64(rand.Int31n(math.MaxInt32)) * time.Now().UnixMilli()
}
