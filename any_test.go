package utils

import (
	"math/rand"
	"testing"
)

type Obj struct {
	id int64
	Any
}

type ObjWithId struct {
	AnyWithId
}

// ObjFromSubProperties is configured from a set of properties that is
// embedded within a higer set (vs. being at the root of the app_config.yaml
// file).
type ObjFromSubProperties struct {
	Any
}

func TestCreateAnyObject(t *testing.T) {
	o := &Obj{
		id: rand.Int63(),
	}
	o.Init("testObj")
	if o.Logger.Name() != "testObj" {
		t.Fatalf("Creating a new object failed: the logger's name %s is not the expected one.", o.Logger.Name())
	}
	oName, err := o.Config.Get("name")
	if err != nil {
		t.Fatalf("Creating a new object failed: the new object does not have property name: %v.", err)
	} else if oName != "Testing" {
		t.Fatalf("Creating a new object failed: the new object property name %s does not have the expected value.", oName)
	}
}

func TestCreateAnyWithIdObject(t *testing.T) {
	t.Log("Testing the creation and initialisation of an object that composes from the AnyWithId struct.")
	o := &ObjWithId{}
	o.Init("testObjWithId")
	if o.Logger.Name() != "testObjWithId" {
		t.Fatalf("Creating a new object failed: the logger's name %s is not the expected one.", o.Logger.Name())
	}
	if o.Id == 0 {
		t.Fatalf("Creating a new object failed: the object identifier is not set.")
	} else {
		t.Log("Created object identifier:", o.Id)
	}
}

func TestCreateAnyObjectFromSubProperties(t *testing.T) {
	t.Log("Testing the creation and initialisation of an object which properties are sub-properties in the configuration file.")
	o := &ObjFromSubProperties{}
	o.Init("setup", "objects", "testObjFromSubProperties")
	oName, err := o.Config.Get("name")
	if err != nil {
		t.Fatalf("Creating a new object failed: the new object does not have property name: %v.", err)
	} else if oName != "TestingObjFromSubProperties" {
		t.Fatalf("Creating a new object failed: the new object property name %s does not have the expected value.", oName)
	}
}

func TestCreateAnyObjectFromConfig(t *testing.T) {
	t.Log("Testing the creation and initialisation of an object which properties are wrapped into a Config instance.")
	o := &ObjFromSubProperties{}
	o.InitWithConfig(*GetConfigForTag("setup", "objects", "testObjFromSubProperties"))
	oName, err := o.Config.Get("name")
	if err != nil {
		t.Fatalf("Creating a new object failed: the new object does not have property name: %v.", err)
	} else if oName != "TestingObjFromSubProperties" {
		t.Fatalf("Creating a new object failed: the new object property name %s does not have the expected value.", oName)
	}
	oLoggerName := o.Logger.Name()
	if oLoggerName == UNSET_LOGGER_NAME {
		t.Fatalf("Creating a new object failed: the new object logger setup has not been set from the given configuration.")
	} else {
		o.Logger.Info("TestCreateAnyObjectFromConfig", "ALL GOOD!")
	}
}
