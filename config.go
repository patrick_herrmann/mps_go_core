/*
utils package provides a way to load a JSON-formated configuration into a
Config struct in Go, so that it's easy to refer to the properties and their
values set in a JSON-formated configuration file.

The main type is obviously Config, which provides all necessary features to
load the content of a JSON configuration file, unmarshal it and get
(and even set) values of properties by their key.
*/
package utils

import (
	"errors"
	"fmt"
	"io"
	"os"

	yaml "gopkg.in/yaml.v2"
)

// Config is the type that holds a set of properties provided to configure
// an application.
//
// It should be retrieved through method GetConfig.
type Config struct {
	properties  map[interface{}]interface{}
	initialized bool
}

// config is the global variable that holds the default configuration read
// from the file in the application.
var config = &Config{
	initialized: false,
}

const (
	// DEFAULT_CONFIG_FILE_NAME is the name of the file that holds the set
	// of properties for configuring an application:
	//	app_config.yaml
	DEFAULT_CONFIG_FILE_NAME string = "app_config.yaml"
)

var configFileDefaultPaths = []string{
	os.Getenv("HOME") + string(os.PathSeparator) + DEFAULT_CONFIG_FILE_NAME,
	os.Getenv("PATH") + string(os.PathSeparator) + DEFAULT_CONFIG_FILE_NAME,
	"." + string(os.PathSeparator) + "config" + string(os.PathSeparator) + DEFAULT_CONFIG_FILE_NAME,
	"." + string(os.PathSeparator) + DEFAULT_CONFIG_FILE_NAME,
}

// PropertyNotFoundError is raised if a given key expected to match a
// property in a Config does not exist (see function Config.Get).
//
// It implements the error interface by defining function:
//	Error() string
type PropertyNotFoundError struct {
	key     string
	subkeys []string
}

func (err *PropertyNotFoundError) Error() string {
	if len(err.subkeys) > 0 {
		return fmt.Sprintf("Property %s:%v does not exist.", err.key, err.subkeys)
	} else {
		return fmt.Sprintf("Property %s does not exist.", err.key)
	}
}

/*
GetConfig returns a Config instance that's initialized with the default set
of configuration properties for this app.

The default set of configuration properties are expected to be found in
either of those paths (in that order):
	. $HOME/app_config.yaml
	. $PATH/app_config.yaml
	. ./config/app_config.yaml
	. ./app_config.yaml
If there is no such file, this function returns a configuration instance
with no properties.
*/
func GetConfig() *Config {
	if !config.initialized {
		for _, path := range configFileDefaultPaths {
			if _, err := os.Stat(path); errors.Is(err, os.ErrNotExist) {
				fmt.Println("GetConfig(path", path, "does not exist)")
			} else {
				if configFile, err := os.Open(path); err != nil {
					fmt.Println("GetConfig(cannot open the configuration file at", path, ":", err, ")")
				} else if configProps, err := io.ReadAll(configFile); err != nil {
					fmt.Println("GetConfig(cannot read the configuration file at", path, ":", err, ")")
				} else {
					fmt.Println("GetConfig(reading the configuration properties from file", path, ")")
					if err := config.Unmarshal(configProps); err != nil {
						fmt.Println("GetConfig(could not read the configuration properties from file", path, ":", err, ")")
					}
					break
				}
			}
		}
		config.initialized = true
	}
	return config
}

/*
GetConfigForTag looks, within the default set of configuration properties,
if any matches the given tag and subtags (if any are specified). If yes, it
creates a new Config instance with that subset of properties and returns it.

If there is no property matching the given tag, then an empty configuration
is returned. It can still be used to attach additional properties if needed.
*/
func GetConfigForTag(tag string, subtags ...string) *Config {
	mcf := GetConfig()
	cf := &Config{
		initialized: true,
		properties:  make(map[interface{}]interface{}),
	}
	if value, err := mcf.Get(tag, subtags...); err == nil {
		if m, ok := value.(map[interface{}]interface{}); ok {
			cf.properties = m
		}
	}
	return cf
}

/*
GetConfigForTags looks, in the given configurations set, if there is a match
for the given tags (reflecting the tree where to find the subset). If yes,
it returns a new Config with that specific subset of properties. Otherwhise
it returns an empty configurations set.
*/
func (cfg *Config) GetConfigForTags(tags ...string) *Config {
	cf := &Config{
		initialized: true,
		properties:  make(map[interface{}]interface{}),
	}
	var value interface{}
	var err error
	if len(tags) > 1 {
		value, err = cfg.Get(tags[0], tags[1:]...)
	} else if len(tags) > 0 {
		value, err = cfg.Get(tags[0])
	}
	if err == nil {
		if m, ok := value.(map[interface{}]interface{}); ok {
			cf.properties = m
		}
	}
	return cf
}

/*
Unmarshal decodes a set of configuration properties into the Config
instance for later use.

The given array of bytes (data) can be encoded in yaml; this function uses
this module: https://pkg.go.dev/gopkg.in/yaml.v2#section-readme.

The resulting properties are merged with the already existing ones in the
Config instance.
*/
func (cf *Config) Unmarshal(data []byte) error {
	m := make(map[interface{}]interface{})
	// Unmarshal the new set of properties into a temporary map and merge
	// that map w/ the existing one for the Config instance.
	if err := yaml.Unmarshal(data, &m); err != nil {
		return err
	} else {
		if cf.properties == nil {
			cf.properties = make(map[interface{}]interface{})
		}
		for key, val := range m {
			cf.properties[key] = val
		}
	}
	return nil
}

/*
Get returns the value that is mapped with the given key in the configured
set of properties.

subkeys is a 0-many length list of extra keys, that are actually reflecting
nested properties in the original yaml configuration file. For example, to
retrive the value of property prop1/prop1.2/prop1.2.1 of the following
configuration:
	prop1:
		prop1.1: "foo"
		prop1.2:
			prop1.2.1: "bar"
this method should be called as follow:
	val, err := config.Get("prop1", "prop1.2", "prop1.2.1")

If key or any of the subkeys does not exist, this function raises the
PropertyNotFoundError error.
*/
func (cf *Config) Get(key string, subkeys ...string) (interface{}, error) {
	if cf.properties == nil {
		return nil, fmt.Errorf("No properties")
	}
	if value, ok := cf.properties[key]; ok {
		// If there are some subkeys: let's find the exact property.
		for _, subkey := range subkeys {
			if m, ok := value.(map[interface{}]interface{}); ok {
				if value, ok = m[subkey]; !ok {
					return nil, &PropertyNotFoundError{key: key, subkeys: subkeys}
				}
			}
		}
		return value, nil
	} else {
		return nil, &PropertyNotFoundError{key: key}
	}
}

/*
GetWithFallback returns a value attached to a given property, as function
Get does, but does not raise an error if the property does not exist. It
rather sends back the given fallback value.
*/
func (cf *Config) GetWithFallback(fallback interface{}, key string, subkeys ...string) interface{} {
	if value, err := cf.Get(key, subkeys...); err != nil {
		return fallback
	} else {
		return value
	}
}

/*
HasProperties checks if there are at least one property attached to the
Config and returns true if that's the case.
*/
func (cf *Config) HasProperties() bool {
	return len(cf.properties) > 0
}
