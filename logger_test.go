package utils

import (
	"testing"
)

func TestInfo(t *testing.T) {
	t.Log("Testing logging info level message")
	logger := GetLogger("Test")
	logger.Info("TestInfo", "This is a test message")
	logger.Info("TestInfo", "This is a test message w/ an integer  parameter: %d", 123)
}

func TestChangeLoggerName(t *testing.T) {
	t.Log("Testing changing a logger's name")
	logger := GetLogger("Test")
	if logger.Name() != "Test" {
		t.Errorf("Logger name %s is not the right one.", logger.Name())
	}
	logger.Name("NewTest")
	if logger.Name() != "NewTest" {
		t.Errorf("New logger name %s is not the right one.", logger.Name())
	}
}
